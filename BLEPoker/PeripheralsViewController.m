//
//  PeripheralsViewController.m
//  DropScale
//
//  Created by Zakk Hoyt on 2/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import "PeripheralsViewController.h"
#import "ServicesViewController.h"
#import "BLEManager.h"
#import "AppDelegate.h"

@import CoreBluetooth;
@import QuartzCore;

NSString *SeguePeripheralsToServices = @"SeguePeripheralsToServices";

@interface PeripheralsViewController ()<UITableViewDataSource, UITableViewDelegate>
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation PeripheralsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Peripherals";
    self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);

    NSLog(@"%s:", __FUNCTION__);

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidDiscoverPeripheral object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        [self.tableView reloadData];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidConnectPeripheral object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBPeripheral *peripheral = note.object;
        [self performSegueWithIdentifier:@"SeguePeripheralsToServices" sender:peripheral];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SeguePeripheralsToServices]) {
        ServicesViewController *vc = segue.destinationViewController;
        vc.peripheral = sender;
    }
}

- (IBAction)startButtonAction:(id)sender {
    NSLog(@"%s:", __FUNCTION__);
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.bleManager startScan];
}

- (IBAction)stopButtonAction:(id)sender {
    NSLog(@"%s:", __FUNCTION__);

    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.bleManager stopScan];
}

#pragma mark - CBCharacteristic helpers

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    return appDelegate.bleManager.peripherals.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PeripheralCell"];
    NSString *key = appDelegate.bleManager.peripherals.allKeys[indexPath.item];
    CBPeripheral *peripheral = appDelegate.bleManager.peripherals[key];
    cell.textLabel.text = peripheral.name;
    cell.detailTextLabel.text = peripheral.identifier.UUIDString;
    return cell;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *uuid = appDelegate.bleManager.peripherals.allKeys[indexPath.item];
    CBPeripheral *peripheral = appDelegate.bleManager.peripherals[uuid];
    [appDelegate.bleManager.centralManager connectPeripheral:peripheral options:nil];
}

@end
