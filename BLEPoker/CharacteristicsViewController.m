//
//  CharacteristicsViewController.m
//  DropScale
//
//  Created by Zakk Hoyt on 2/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import "CharacteristicsViewController.h"
#import "AppDelegate.h"
#import "BLEManager.h"

@interface CharacteristicsViewController ()

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation CharacteristicsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Characteristics";

    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.bleManager discoverCharacteristicsForService:_service];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidDiscoverCharacteristicsForService object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBService *service = note.object;
        _characteristics = service.characteristics;
        [self.tableView reloadData];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidUpdateValueForCharacteristic object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        [self.tableView reloadData];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidUpdateNotificationStateForCharacteristic object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        [self.tableView reloadData];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    // Clean up
    for (CBCharacteristic *c in _characteristics) {
        if (c.properties & CBCharacteristicPropertyNotify) {
            [_peripheral setNotifyValue:NO forCharacteristic:c];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSString *)stringForProperties:(CBCharacteristicProperties)properties {
    //    CBCharacteristicPropertyBroadcast												= 0x01,
    //    CBCharacteristicPropertyRead													= 0x02,
    //    CBCharacteristicPropertyWriteWithoutResponse									= 0x04,
    //    CBCharacteristicPropertyWrite													= 0x08,
    //    CBCharacteristicPropertyNotify													= 0x10,
    //    CBCharacteristicPropertyIndicate												= 0x20,
    //    CBCharacteristicPropertyAuthenticatedSignedWrites								= 0x40,
    //    CBCharacteristicPropertyExtendedProperties										= 0x80,
    //    CBCharacteristicPropertyNotifyEncryptionRequired NS_ENUM_AVAILABLE(NA, 6_0)		= 0x100,
    //    CBCharacteristicPropertyIndicateEncryptionRequired NS_ENUM_AVAILABLE(NA, 6_0)	= 0x200

    NSMutableString *p = [NSMutableString new];
    if (properties & CBCharacteristicPropertyBroadcast) {
        [p appendFormat:@"broadcast, "];
    }
    if (properties & CBCharacteristicPropertyRead) {
        [p appendFormat:@"read, "];
    }
    if (properties & CBCharacteristicPropertyWriteWithoutResponse) {
        [p appendFormat:@"write without response, "];
    }
    if (properties & CBCharacteristicPropertyWrite) {
        [p appendFormat:@"write, "];
    }
    if (properties & CBCharacteristicPropertyNotify) {
        [p appendFormat:@"notify, "];
    }
    if (properties & CBCharacteristicPropertyIndicate) {
        [p appendFormat:@"indicate, "];
    }
    if (properties & CBCharacteristicPropertyAuthenticatedSignedWrites) {
        [p appendFormat:@"authenticate signed writes, "];
    }
    if (properties & CBCharacteristicPropertyExtendedProperties) {
        [p appendFormat:@"extended properties, "];
    }
    if (properties & CBCharacteristicPropertyNotifyEncryptionRequired) {
        [p appendFormat:@"notify encryption required, "];
    }
    if (properties & CBCharacteristicPropertyIndicateEncryptionRequired) {
        [p appendFormat:@"indicate encryption required, "];
    }
    return p;
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _characteristics.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CharacteristicCell"];
    CBCharacteristic *c = _characteristics[indexPath.item];
    //    NSString *value = [[NSString alloc] initWithData:c.value encoding:NSUTF8StringEncoding];
    //    CBDescriptor *d = [c.descriptors firstObject];
    //    NSString *value = [[NSString alloc] initWithData:d.value encoding:NSUTF8StringEncoding];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", c.UUID.description, c.value];
    cell.detailTextLabel.text = [self stringForProperties:c.properties];
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CBCharacteristic *c = _characteristics[indexPath.item];
    if (c.properties & CBCharacteristicPropertyRead) {
        [_peripheral readValueForCharacteristic:c];
    } else if (c.properties & CBCharacteristicPropertyNotify) {
        [_peripheral setNotifyValue:YES forCharacteristic:c];
    }
}

@end
