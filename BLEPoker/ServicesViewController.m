//
//  ServicesViewController.m
//  DropScale
//
//  Created by Zakk Hoyt on 2/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import "ServicesViewController.h"
#import "CharacteristicsViewController.h"
#import "AppDelegate.h"
#import "BLEManager.h"

NSString *SegueServicesToCharacteristics = @"SegueServicesToCharacteristics";

@interface ServicesViewController ()<CBPeripheralDelegate, UITableViewDataSource, UITableViewDelegate>
@property(weak, nonatomic) IBOutlet UITableView *tableView;
//@property(nonatomic, strong) NSMutableArray *services;
@end

@implementation ServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        self.navigationItem.title = @"Services";

    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.bleManager discoverServicesForPeripheral:_peripheral];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidDiscoverServices object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        [self.tableView reloadData];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SegueServicesToCharacteristics]) {
        CBService *service = sender;
        CharacteristicsViewController *vc = segue.destinationViewController;
        vc.peripheral = _peripheral;
        vc.service = service;
    }
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _peripheral.services.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell"];
    CBService *service = _peripheral.services[indexPath.item];
    cell.textLabel.text = service.UUID.UUIDString;
    cell.detailTextLabel.text = service.description;
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CBService *service = _peripheral.services[indexPath.row];
    NSArray<CBCharacteristic *> *characteristics = service.characteristics;
    [self performSegueWithIdentifier:SegueServicesToCharacteristics sender:service];
}

@end
