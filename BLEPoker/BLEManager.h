//
//  BLEManager.h
//  DropScale
//
//  Created by Zakk Hoyt on 2/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreBluetooth;

static NSString *BLEManagerDidDiscoverPeripheral = @"BLEManagerDidDiscoverPeripheral";
static NSString *BLEManagerDidConnectPeripheral = @"BLEManagerDidConnectPeripheral";
static NSString *BLEManagerDidUpdateState = @"BLEManagerDidUpdateState";
static NSString *BLEManagerDidDiscoverPeripherals = @"BLEManagerDidDiscoverPeripherals";
static NSString *BLEManagerDidDiscoverServices = @"BLEManagerDidDiscoverServices";
static NSString *BLEManagerDidDiscoverCharacteristicsForService = @"BLEManagerDidDiscoverCharacteristicsForService";
static NSString *BLEManagerDidUpdateValueForCharacteristic = @"BLEManagerDidUpdateValueForCharacteristic";
static NSString *BLEManagerDidUpdateNotificationStateForCharacteristic = @"BLEManagerDidUpdateNotificationStateForCharacteristic";

@interface BLEManager : NSObject
@property(nonatomic, strong, readonly) CBCentralManager *centralManager;
@property(nonatomic, strong, readonly) NSMutableDictionary *peripherals;

-(void)startScan;
-(void)startScanForServices:(nullable NSArray<NSString *> *)services;
-(void)stopScan;
-(void)discoverServicesForPeripheral:(CBPeripheral*)peripheral;
-(void)discoverCharacteristicsForService:(CBService*)service;
@end
