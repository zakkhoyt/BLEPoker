//
//  DropViewController.m
//  BLEPoker
//
//  Created by Zakk Hoyt on 2/28/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import "DropViewController.h"
#import "BLEManager.h"
#import "AppDelegate.h"

// Glass Cup
// 0x2ef5

// Zero
// 0x1378

// 0x1B7D

// 7037 / 688
// 10.2281976744 ticks per gram

NSString *DropScalePeripheralUUID = @"DCECE6D5-0373-5AEC-53DE-E1A70C891E03";
NSString *DropScaleServiceUUID = @"ADA3";
NSString *DropScaleCharacteristicWeight01 = @"FF03";
NSString *DropScaleCharacteristicWeight02 = @"FF09";

NSString *DropScaleZeroSensorOffset = @"DropScaleZeroSensorOffset";

//const float kDivisor = 10.2281976744;
const float kDivisor = 10.0;

@interface DropViewController ()
//@property(nonatomic, strong) CBPeripheral *peripheral;
@property(weak, nonatomic) IBOutlet UILabel *weightLabel;
@property(weak, nonatomic) IBOutlet UIButton *zeroButton;
@property(nonatomic) NSInteger currentSensorValue;
@property(nonatomic) NSInteger zeroSensorValue;
@end

@implementation DropViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.weightLabel.text = @"Connecting...";
    self.zeroButton.hidden = YES;

    NSNumber *sensorOffset = [[NSUserDefaults standardUserDefaults] objectForKey:DropScaleZeroSensorOffset];
    if (sensorOffset) {
        self.zeroSensorValue = sensorOffset.integerValue;
    }

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidDiscoverPeripheral object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBPeripheral *peripheral = note.object;
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate.bleManager.centralManager connectPeripheral:peripheral options:nil];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidConnectPeripheral object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBPeripheral *peripheral = note.object;
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate.bleManager discoverServicesForPeripheral:peripheral];
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidDiscoverServices object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBPeripheral *peripheral = note.object;

        [peripheral.services enumerateObjectsUsingBlock:^(CBService *_Nonnull service, NSUInteger idx, BOOL *_Nonnull stop) {
            if ([service.UUID.UUIDString isEqualToString:DropScaleServiceUUID]) {
                [peripheral discoverCharacteristics:nil forService:service];
            }
        }];

    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidDiscoverCharacteristicsForService object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBService *service = note.object;
        [service.characteristics enumerateObjectsUsingBlock:^(CBCharacteristic *_Nonnull c, NSUInteger idx, BOOL *_Nonnull stop) {
            NSLog(@"Enum char: %@", c.description);
            if ([self characteristic:c isString:DropScaleCharacteristicWeight01]) {
                [service.peripheral setNotifyValue:YES forCharacteristic:c];
            } else if ([self characteristic:c isString:DropScaleCharacteristicWeight02]) {
                //                [service.peripheral setNotifyValue:YES forCharacteristic:c];
            }
        }];

    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidUpdateNotificationStateForCharacteristic object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBCharacteristic *c = note.object;
        if ([self characteristic:c isString:DropScaleCharacteristicWeight01]) {
            NSLog(@"c1: %@", c.value);
            [self updateLabelWithCharacteristic:c];
        } else if ([self characteristic:c isString:DropScaleCharacteristicWeight02]) {
            NSLog(@"c2: %@", c.value);
        }
    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:BLEManagerDidUpdateValueForCharacteristic object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *_Nonnull note) {
        CBCharacteristic *c = note.object;
        if ([self characteristic:c isString:DropScaleCharacteristicWeight01]) {
            NSLog(@"c1: %@", c.value);
            [self updateLabelWithCharacteristic:c];
        } else if ([self characteristic:c isString:DropScaleCharacteristicWeight02]) {
            NSLog(@"c2: %@", c.value);
        }
    }];
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (BOOL)characteristic:(CBCharacteristic *)characteristic isString:(NSString *)string {
    NSString *uuid = (NSString *)[NSString stringWithFormat:@"%@", characteristic.UUID.UUIDString];
    if ([uuid isEqualToString:string]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)updateLabelWithCharacteristic:(CBCharacteristic *)characteristic {
    // Get the Heart Rate Monitor BPM
    if (characteristic.value) {
        self.weightLabel.text = @"";
        self.zeroButton.hidden = NO;

        NSData *data = [characteristic value];  // 1
        const uint8_t *reportData = [data bytes];
        NSUInteger sensor = reportData[2] * 256 + reportData[3];
        NSLog(@"sensor: %lu", (unsigned long)sensor);

        self.currentSensorValue = sensor;

        [self updateWeightLabel];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startScan];
    });
}


- (void)updateWeightLabel {
    float displaySensorValue = self.currentSensorValue - self.zeroSensorValue;
    displaySensorValue /= kDivisor;
    self.weightLabel.text = [NSString stringWithFormat:@"%.2f grams", displaySensorValue];
}

- (IBAction)zeroButtonAction:(id)sender {
    self.zeroSensorValue = self.currentSensorValue;
    [self updateWeightLabel];

    [[NSUserDefaults standardUserDefaults] setObject:@(self.zeroSensorValue) forKey:DropScaleZeroSensorOffset];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)startScan {
    NSLog(@"%s:", __FUNCTION__);
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.bleManager startScanForServices:@[ DropScaleServiceUUID ]];
}

@end
