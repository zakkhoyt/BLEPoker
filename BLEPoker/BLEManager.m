//
//  BLEManager.m
//  DropScale
//
//  Created by Zakk Hoyt on 2/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import "BLEManager.h"

@interface BLEManager ()<CBCentralManagerDelegate, CBPeripheralDelegate>
@property(nonatomic, strong, readwrite) CBCentralManager *centralManager;
@property(nonatomic, strong, readwrite) NSMutableDictionary *peripherals;
@end

@implementation BLEManager

- (instancetype)init {
    self = [super init];
    if (self) {
        self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        _peripherals = [@{} mutableCopy];
    }
    return self;
}

- (void)startScan {
    [_centralManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)startScanForServices:(nullable NSArray<NSString *> *)services {
    // Convert strings to uuids
    NSMutableArray<CBUUID *> *uuids = [@[] mutableCopy];
    [services enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        CBUUID *uuid = [CBUUID UUIDWithString:obj];
        [uuids addObject:uuid];
    }];

    [_centralManager scanForPeripheralsWithServices:uuids options:nil];
}
- (void)stopScan {
    [_centralManager stopScan];
}
- (void)discoverServicesForPeripheral:(CBPeripheral *)peripheral {
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
}

- (void)discoverCharacteristicsForService:(CBService *)service {
    [service.peripheral discoverCharacteristics:nil forService:service];
}

- (void)addPeripheral:(CBPeripheral *)peripheral {
    NSString *key = peripheral.identifier.UUIDString;
    _peripherals[key] = peripheral;
}

#pragma mark - CBCentralManagerDelegate

// CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s: Discovered peripheral: %@", __FUNCTION__, peripheral.description);
    [self addPeripheral:peripheral];
    [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidDiscoverPeripheral object:peripheral];
}

// method called whenever you have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s:", __FUNCTION__);

    if (peripheral.state == CBPeripheralStateConnected) {
        [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidConnectPeripheral object:peripheral];
    } else {
        NSLog(@"ERROR: Can't get services becaues we are not connected");
    }
}

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s:", __FUNCTION__);

    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
    } else if ([central state] == CBCentralManagerStatePoweredOn) {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
    } else if ([central state] == CBCentralManagerStateUnauthorized) {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
    } else if ([central state] == CBCentralManagerStateUnknown) {
        NSLog(@"CoreBluetooth BLE state is unknown");
    } else if ([central state] == CBCentralManagerStateUnsupported) {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidUpdateState object:central];
}

#pragma mark - CBPeripheralDelegate

// CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(nullable NSError *)error {
    NSLog(@"%s:", __FUNCTION__);

    [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidDiscoverServices object:peripheral];
}

// Invoked when you discover the characteristics of a specified service.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(nullable NSError *)error {
    NSLog(@"%s:", __FUNCTION__);

    for (CBCharacteristic *c in service.characteristics) {
        NSLog(@"Discovered characteristic: %@", c.UUID);
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidDiscoverCharacteristicsForService object:service];
}

// Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error {
    NSLog(@"%s:", __FUNCTION__);
    [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidUpdateValueForCharacteristic object:characteristic];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error {
    NSLog(@"%s:", __FUNCTION__);
    [[NSNotificationCenter defaultCenter] postNotificationName:BLEManagerDidUpdateNotificationStateForCharacteristic object:characteristic];
}
@end
