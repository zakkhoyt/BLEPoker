//
//  CharacteristicsViewController.h
//  DropScale
//
//  Created by Zakk Hoyt on 2/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreBluetooth;

@interface CharacteristicsViewController : UIViewController
@property(nonatomic, strong) CBPeripheral *peripheral;
@property(nonatomic, strong) CBService *service;
@property(nonatomic, strong) NSArray<CBCharacteristic *> *characteristics;
@end
